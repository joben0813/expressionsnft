# Expressions Contract

- Mint function takes in string param of comma separated values
- convert to array
- Ranges are checked for each value in array
- string variation is stored and checked to avoid duplicates
- tokenURI retrieves string variation of seed from token data, splits by commas, and returns the int array variation.
- values from array are used to build svg
- seed string is used to pick the fill color of the rect
- seed string and msg.sender are used to pick the stroke color of the rect

## Code

```// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Base64.sol";

contract Expressions is ERC721, Ownable {
    constructor() ERC721("Expressions", "EXP") {}

    mapping(bytes32 => bool) private usedSeeds;
    uint256 private _tokenIdCounter;
    // mapping(uint256 => uint256[]) public tokenData;
    mapping(uint256 => string) public tokenData;

    function safeMint(string memory seed) public onlyOwner {
        uint256[] memory values = parseSeed(seed);
        require(checkRanges(values), "Invalid values");

        bytes32 seedHash = generateSeedHash(values);
        require(!usedSeeds[seedHash], "Seed already used");

        usedSeeds[seedHash] = true;

        uint256 newTokenId = _tokenIdCounter;

        // tokenData[newTokenId] = values;
        tokenData[newTokenId] = seed;


        _tokenIdCounter += 1;

        _safeMint(msg.sender, newTokenId);
    }

    function generateSeedHash(uint256[] memory values) private pure returns (bytes32) {
        return keccak256(abi.encodePacked(values));
    }

    function checkRanges(uint256[] memory values) public pure returns (bool) {
        require(values.length == 24, "Invalid array length");

        // Eyebrow1
        require(values[0] >= 22 && values[0] <= 30, "not in range");
        require(values[1] >= 15 && values[1] <= 25, "not in range");
        require(values[2] >= 35 && values[2] <= 40, "not in range");
        require(values[3] >= 10 && values[3] <= 30, "not in range");
        require(values[4] >= 44 && values[4] <= 46, "not in range");
        require(values[5] >= 20 && values[5] <= 30, "not in range");

        // Eyebrow2
        require(values[6] >= 70 && values[6] <= 78, "not in range");
        require(values[7] >= 15 && values[7] <= 25, "not in range");
        require(values[8] >= 60 && values[8] <= 65, "not in range");
        require(values[9] >= 10 && values[9] <= 30, "not in range");
        require(values[10] >= 54 && values[10] <= 56, "not in range");
        require(values[11] >= 20 && values[11] <= 30, "not in range");

        // Left Eyeball
        require(values[12] >= 31 && values[12] <= 39, "not in range");
        require(values[13] >= 36 && values[13] <= 44, "not in range");
        require(values[14] >= 1 && values[14] <= 7, "not in range");

        // Right Eyeball
        require(values[15] >= 61 && values[15] <= 69, "not in range");
        require(values[16] >= 36 && values[16] <= 44, "not in range");
        require(values[17] >= 1 && values[17] <= 7, "not in range");

        // Mouth
        require(values[18] >= 28 && values[18] <= 40, "not in range");
        require(values[19] >= 68 && values[19] <= 84, "not in range");
        require(values[20] >= 48 && values[20] <= 52, "not in range");
        require(values[21] >= 60 && values[21] <= 87, "not in range");
        require(values[22] >= 60 && values[22] <= 72, "not in range");
        require(values[23] >= 68 && values[23] <= 82, "not in range");

        return true;
    }

    function parseSeed(string memory seed) internal pure returns (uint256[] memory) {
        bytes memory seedBytes = bytes(seed);
        uint256 count = 1;

        for (uint256 i = 0; i < seedBytes.length; i++) {
            if (seedBytes[i] == ",") {
                count++;
            }
        }

        uint256[] memory values = new uint256[](count);
        uint256 index = 0;
        uint256 currentValue = 0;

        for (uint256 i = 0; i < seedBytes.length; i++) {
            if (seedBytes[i] == ",") {
                values[index] = currentValue;
                index++;
                currentValue = 0;
            } else {
                currentValue = currentValue * 10 + (uint256(uint8(seedBytes[i])) - 48);
            }
        }

        values[index] = currentValue;
        return values;
    }

    function generateSVG(string memory seed, uint256 tokenId, string memory fill, string memory stroke) public view returns (string memory) {
        uint256[] memory values = parseSeed(seed);

        // Replace the placeholders with the actual values
        string memory svg = string(
            abi.encodePacked(
                '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100">',
                '<rect width="100" height="100" fill="', fill ,'" stroke="', stroke ,'" stroke-width="3" />',
                // Eyes
                '<circle id="left-eye" cx="35" cy="40" r="12" fill="#fff" stroke="#000" />',
                '<circle id="right-eye" cx="65" cy="40" r="12" fill="#fff" stroke="#000" />',
                // Eyeballs
                '<circle id="left-eyeball" cx="', Strings.toString(values[12]), '" cy="', Strings.toString(values[13]), '" r="', Strings.toString(values[14]), '" fill="#000" />',
                '<circle id="right-eyeball" cx="', Strings.toString(values[15]), '" cy="', Strings.toString(values[16]), '" r="', Strings.toString(values[17]), '" fill="#000" />',
                getMouth(values),
                getEyebrows(values)
            )
        );

        return svg;
    }

    function getEyebrows(uint256[] memory values) public view returns (string memory) {
        string memory svg = string(
            abi.encodePacked(
                // // Eyebrows
                '<path id="eyebrow1" d="M ', Strings.toString(values[0]), " " ,Strings.toString(values[1]),' Q ', Strings.toString(values[2]), " " , Strings.toString(values[3]), " " , Strings.toString(values[4]), " " , Strings.toString(values[5]), '" stroke="#000" stroke-width="5" fill="none" />',
                '<path id="eyebrow2" d="M ', Strings.toString(values[6]), " " ,Strings.toString(values[7]),' Q ', Strings.toString(values[8]), " " , Strings.toString(values[9]), " " , Strings.toString(values[10]), " " , Strings.toString(values[11]), '" stroke="#000" stroke-width="5" fill="none" />',
                '</svg>'
            )
         );
        return svg;
    }

    function getMouth(uint256[] memory values) public view returns (string memory) {
        string memory svg = string(
            abi.encodePacked(
                 // Mouth
                '<path id="mouth" d="M ', Strings.toString(values[18]), " ", Strings.toString(values[19]), ' Q ', Strings.toString(values[20]), " ",Strings.toString(values[21]), " ", Strings.toString(values[22]), " ", Strings.toString(values[23]), " ", '" stroke="#000" stroke-width="5" fill="none" />'
            )
         );
        return svg;
    }

    function svgToImageURI(string memory _source) public pure returns (string memory) {
        string memory baseURL = "data:image/svg+xml;base64,";
        string memory svgBase64Encoded = Base64.encode(bytes(string(abi.encodePacked(_source))));
        return string(abi.encodePacked(baseURL, svgBase64Encoded));
    }


    function formatTokenURI(string memory _imageURI, string memory _name, string memory _description, string memory _properties) public pure returns (string memory) {
        return string(
            abi.encodePacked(
                "data:application/json;base64,",
                Base64.encode(
                    bytes(
                        abi.encodePacked(
                            '{"name":"', _name,
                            '", "description": "', _description, '",', // Add a comma here
                            ' "attributes": [', _properties, ']', // Change the format to be an array of objects
                            ', "image":"', _imageURI, '"}'
                        )
                    )
                )
            )
        );
    }


    function tokenURI(uint256 tokenId) public view override returns (string memory) {
        require(_exists(tokenId), "Token does not exist");

        string memory seed = tokenData[tokenId];

        (string memory fillColor, string memory background) = selectColor(seed, 1);
        (string memory strokeColor, string memory border) = selectColor(seed, 2);


        string memory svg = generateSVG(seed, tokenId, fillColor, strokeColor);

        // Convert the SVG to a base64-encoded data URI
        string memory base64Svg = svgToImageURI(svg);

        // Construct the JSON metadata
        string memory base64Json = formatTokenURI(base64Svg, string(abi.encodePacked("Expression ", Strings.toString(tokenId))), "What will your expression be?", string(
            abi.encodePacked(
                '[',
                '{"trait_type": "background", "value": "', background, '"}',
                ', {"trait_type": "border", "value": "', border, '"}',
                ', {"trait_type": "single-color", "value": "', (keccak256(abi.encodePacked(background)) == keccak256(abi.encodePacked(border))) ? "true" : "false", '"}',
                ']'
            )
        ));


        // Convert the JSON to a base64-encoded data URI
        // string memory base64Json = formatTokenURI(json);

        return base64Json;
    }


    function selectColor(string memory seed, uint8 randomType) public view returns (string memory, string memory) {
        uint256 rand;

        if (randomType == 1) {
            rand = random(1, 100, seed);
        } else {
            rand = randomTwo(1, 100, seed);
        }

        if (rand <= 43) {
            return ("#FFDC00", "yellow");
        } else if (rand <= 68) {
            return ("#0074D9", "blue");
        } else if (rand <= 83) {
            return ("#FF851B", "orange");
        } else if (rand <= 93) {
            return ("#2ECC40", "green");
        } else if (rand <= 98) {
            return ("#B10DC9", "purple");
        } else {
            return ("#FF4136", "red");
        }
    }



    function random(uint256 min, uint256 max, string memory seed) internal view returns (uint256) {
        return uint256(keccak256(abi.encodePacked(seed, msg.sender))) % (max - min) + min;
    }
    function randomTwo(uint256 min, uint256 max, string memory seed) internal view returns (uint256) {
        return uint256(keccak256(abi.encodePacked(seed))) % (max - min) + min;
    }
}


```

